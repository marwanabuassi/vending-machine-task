import {Component, OnInit} from '@angular/core';
import {Product} from './models/product';
import {ControlService} from './services/control.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public productsList: Array<Product>;

  constructor(public controlService: ControlService) {
  }

  ngOnInit() {
    this.productsList = this.controlService.productsList;
  }
}
