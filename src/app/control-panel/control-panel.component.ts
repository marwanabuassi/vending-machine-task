import {Component, OnInit} from '@angular/core';
import {ControlService} from '../services/control.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {
  public selectedProductNumber: number;
  public insertedMoney = 0;

  constructor(public controlService: ControlService) {
  }

  ngOnInit() {
  }

  public onSelectProduct() {
    if (this.selectedProductNumber > 25 || this.selectedProductNumber < 1) {
      alert('Invalid product Number');
      return;
    }
    this.controlService.selectProduct(this.selectedProductNumber - 1);
  }

  public onInsertMoney() {
    if (this.controlService.isAddedMoneyValid(this.insertedMoney)) {
      this.controlService.insertMoney(this.insertedMoney);
      this.insertedMoney = 0;
    }
  }

  public buyProduct() {
    if (this.controlService.canPurchaseSelectedProduct()) {
      this.controlService.purchaseTheProduct(this.selectedProductNumber - 1);
      this.insertedMoney = 0;
      this.selectedProductNumber = null;
    }
  }

  public onClear() {
    this.controlService.clear();
    this.insertedMoney = 0;
    this.selectedProductNumber = null;
  }
}
