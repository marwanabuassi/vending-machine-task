export class Product {
 public name: string;
 public price: number;
 public id: string;
 public img: string;
 public quantity: number;
}
