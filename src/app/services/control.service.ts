import {Injectable} from '@angular/core';
import {Product} from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ControlService {
  public selectedProduct: Product;
  public acceptedMoney = [0.1, 0.2, 0.5, 1, 20, 50];
  public totalAddedMoney = 0;
  public remainingMoney = 0;
  // mock data
  public productsList: Array<Product> = [
    {name: 'p', id: '01', price: 3.7, quantity: 12, img: 'assets/images/p6.png'},
    {name: 'p1', id: '02', price: 4.7, quantity: 2, img: 'assets/images/p2.png'},
    {name: 'p2', id: '03', price: 5.7, quantity: 5, img: 'assets/images/p3.png'},
    {name: 'p3', id: '04', price: 2.7, quantity: 1, img: 'assets/images/p4.png'},
    {name: 'p4', id: '05', price: 1.7, quantity: 2, img: 'assets/images/p5.png'},
    {name: 'p5', id: '06', price: 2.7, quantity: 5, img: 'assets/images/p6.png'},
    {name: 'p6', id: '07', price: 4.7, quantity: 4, img: 'assets/images/p2.png'},
    {name: 'p7', id: '08', price: 1.7, quantity: 6, img: 'assets/images/p3.png'},
    {name: 'p8', id: '09', price: 0.8, quantity: 2, img: 'assets/images/p4.png'},
    {name: 'p9', id: '10', price: 0.5, quantity: 3, img: 'assets/images/p5.png'},
    {name: 'p10', id: '11', price: 2.0, quantity: 4, img: 'assets/images/p6.png'},
    {name: 'p11', id: '12', price: 8.7, quantity: 7, img: 'assets/images/ali-baba.png'},
    {name: 'p12', id: '13', price: 4.7, quantity: 3, img: 'assets/images/p2.png'},
    {name: 'p13', id: '14', price: 3, quantity: 5, img: 'assets/images/p5.png'},
    {name: 'p14', id: '15', price: 1, quantity: 5, img: 'assets/images/p6.png'},
    {name: 'p15', id: '16', price: 1, quantity: 12, img: 'assets/images/p3.png'},
    {name: 'p16', id: '17', price: 1, quantity: 12, img: 'assets/images/p4.png'},
    {name: 'p17', id: '18', price: 3, quantity: 12, img: 'assets/images/ali-baba.png'},
    {name: 'p18', id: '19', price: 4, quantity: 12, img: 'assets/images/p5.png'},
    {name: 'p19', id: '20', price: 3, quantity: 12, img: 'assets/images/p2.png'},
    {name: 'p20', id: '21', price: 5, quantity: 12, img: 'assets/images/p3.png'},
    {name: 'p21', id: '22', price: 3.7, quantity: 12, img: 'assets/images/p4.png'},
    {name: 'p22', id: '23', price: 2.7, quantity: 12, img: 'assets/images/ali-baba.png'},
    {name: 'p23', id: '24', price: 1.7, quantity: 12, img: 'assets/images/p6.png'},
    {name: 'p24', id: '25', price: 4.7, quantity: 1, img: 'assets/images/p6.png'},

  ];

  public isAddedMoneyValid(money) {
    if (this.acceptedMoney.indexOf(money) < 0) {
      alert('Invalid Money');
    }
    return this.acceptedMoney.indexOf(money) > -1;
  }

  public insertMoney(money) {
    this.totalAddedMoney += money;
  }

  public selectProduct(index) {
    this.selectedProduct = this.productsList[index];
  }

  public canPurchaseSelectedProduct() {
    if (!this.selectedProduct) {
      alert('Please select a product');
      return false;
    }
    if (this.selectedProduct.price > this.totalAddedMoney) {
      alert('Money Is not enough');
    } else if (this.selectedProduct.quantity < 1) {
      alert('The Product is not available');
    }
    return this.selectedProduct.quantity > 0 && this.selectedProduct.price < this.totalAddedMoney;
  }

  public purchaseTheProduct(index) {
    this.productsList[index].quantity--;
    this.remainingMoney = this.totalAddedMoney - this.productsList[index].price;
    alert('Purchased Successfully');
    this.clear(false);
  }

  public clear(resetRemainedMoney = true) {
    this.totalAddedMoney = 0;
    this.selectedProduct = null;
    if (resetRemainedMoney) {
      this.remainingMoney = 0;
    }
  }
}
