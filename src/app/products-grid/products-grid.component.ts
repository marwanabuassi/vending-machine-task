import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../models/product';

@Component({
  selector: 'app-products-grid',
  templateUrl: './products-grid.component.html',
  styleUrls: ['./products-grid.component.css']
})
export class ProductsGridComponent implements OnInit {
  @Input() productsList: Array<Product>;
  @Output() selectProduct: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  onSelectProduct(index) {
    this.selectProduct.emit(index);
  }

}
