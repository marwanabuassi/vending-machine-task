describe('Vending Machine full scenario', () => {
  const PRODUCTS = [
    { name: 'p', id: '01', price: 3.7, quantity: 12, img: 'assets/images/p6.png' },
    { name: 'p1', id: '02', price: 4.7, quantity: 2, img: 'assets/images/p2.png' },
    { name: 'p2', id: '03', price: 5.7, quantity: 5, img: 'assets/images/p3.png' },
    { name: 'p3', id: '04', price: 2.7, quantity: 1, img: 'assets/images/p4.png' },
    { name: 'p4', id: '05', price: 1.7, quantity: 2, img: 'assets/images/p5.png' },
    { name: 'p5', id: '06', price: 2.7, quantity: 5, img: 'assets/images/p6.png' },
    { name: 'p6', id: '07', price: 4.7, quantity: 4, img: 'assets/images/p2.png' },
    { name: 'p7', id: '08', price: 1.7, quantity: 6, img: 'assets/images/p3.png' },
    { name: 'p8', id: '09', price: 0.8, quantity: 2, img: 'assets/images/p4.png' },
    { name: 'p9', id: '10', price: 0.5, quantity: 3, img: 'assets/images/p5.png' },
    { name: 'p10', id: '11', price: 2.0, quantity: 4, img: 'assets/images/p6.png' },
    { name: 'p11', id: '12', price: 8.7, quantity: 7, img: 'assets/images/ali-baba.png' },
    { name: 'p12', id: '13', price: 4.7, quantity: 3, img: 'assets/images/p2.png' },
    { name: 'p13', id: '14', price: 3, quantity: 5, img: 'assets/images/p5.png' },
    { name: 'p14', id: '15', price: 1, quantity: 5, img: 'assets/images/p6.png' },
    { name: 'p15', id: '16', price: 1, quantity: 12, img: 'assets/images/p3.png' },
    { name: 'p16', id: '17', price: 1, quantity: 12, img: 'assets/images/p4.png' },
    { name: 'p17', id: '18', price: 3, quantity: 12, img: 'assets/images/ali-baba.png' },
    { name: 'p18', id: '19', price: 4, quantity: 12, img: 'assets/images/p5.png' },
    { name: 'p19', id: '20', price: 3, quantity: 12, img: 'assets/images/p2.png' },
    { name: 'p20', id: '21', price: 5, quantity: 12, img: 'assets/images/p3.png' },
    { name: 'p21', id: '22', price: 3.7, quantity: 12, img: 'assets/images/p4.png' },
    { name: 'p22', id: '23', price: 2.7, quantity: 12, img: 'assets/images/ali-baba.png' },
    { name: 'p23', id: '24', price: 1.7, quantity: 12, img: 'assets/images/p6.png' },
    { name: 'p24', id: '25', price: 4.7, quantity: 1, img: 'assets/images/p6.png' },

  ]

  before(() => {
    cy.visit('https://marwan-vending-machine-task.web.app/')
    cy.contains('Snack Vending Machine', { timeout: 10000 })
  })
  it('Verify Vending machine products and control panned labels', () => {
    cy.get('div')
      .contains('Snack Vending Machine', { timeout: 5000 })
      .should('be.visible')
    cy.get('.grid-container')
      .should('be.visible')
    for (let product of PRODUCTS) {
      cy.get('div')
        .contains(product.id, { timeout: 5000 })
        .should('be.visible')
      cy.wait(200)
    }
  })
  it('Purchase product from the machine', () => {
    // Add product number
    cy.get('.number-input')
      .type('18')
    // Click outside
    cy.contains('Selected Product Status:')
      .click()
    // Verify selected prooduct fetails
    cy.contains('Available: Yes, 3 $')
      .should('be.visible')
    // Enter 20& to the  machine
    cy.get('.money-input')
      .type(20)
    cy.get('.enter-btn')
      .click()
    // Verify the money added to the machine
    verifyInputValue('.added-money-input', '20')
    // buy product
    cy.get('.buy-btn')
      .click()
    cy.get('.added-money-input')
      .last()
      .should(($input) => {
        const val = $input.val()

        expect(val).to.equal('17')
      })
  })

})
const verifyInputValue = ((selector, inputValue) => {
  cy.get(selector).should(($input) => {
    const val = $input.val()

    expect(val).to.equal(inputValue)
  })
})
